﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    public static class AllControls
    {
        public static List<Control> controls = new List<Control>();
    }

    interface Command
    {
        void Execute();
    }

    public class HideAllComponentsCommand : Command
    {
        public void Execute()
        {
            for (int i = 0; i < AllControls.controls.Count; i++)
            {
                if (Convert.ToString(AllControls.controls[i].Tag) != "DoNotHide")
                {
                    AllControls.controls[i].Visible = false;

                }
            }
        }
    }

    public class ShowComponentsCommand : Command
    {
        private string command = String.Empty;

        public ShowComponentsCommand(string _command)
        {
            command = _command;
        }

        public void Execute()
        {
            for (int i = 0; i < AllControls.controls.Count; i++)
            {
                if (Convert.ToString(AllControls.controls[i].Tag) == command /*&& Convert.ToString(AllControls.controls[i].Tag) != "DoNotHide"*/)
                {
                    AllControls.controls[i].Visible = !AllControls.controls[i].Visible;
                }
            }
        }
    }
    
    public class UpdateCommand : Command
    {
        public void Execute()
        {
            for (int i = 0; i < AllControls.controls.Count; i++)
            {
                if (Convert.ToString(AllControls.controls[i].Name) == "labelLevel")
                {
                    AllControls.controls[i].Text = "Level: " + Convert.ToString(Student.GetCurrentStudent().Level);
                    
                }
                else if (Convert.ToString(AllControls.controls[i].Name) == "labelPoints")
                {
                    AllControls.controls[i].Text = "Points: " + Convert.ToString(Student.GetCurrentStudent().GetInventory().Points);
                }
            }
        }
    }

}
