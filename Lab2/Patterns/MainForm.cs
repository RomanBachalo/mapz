using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace Patterns
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        
        private void CurrentMonthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            MessageBox.Show(Calendar.GetCalendar().Dates[CurrentMonthCalendar.SelectionRange.Start.Day - 1].GetStatus());

        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            
            foreach (Control c in this.Controls)
            {
                AllControls.controls.Add(c);
            }

            Command update = new UpdateCommand();
            Command hideAll = new HideAllComponentsCommand();

            update.Execute();
            hideAll.Execute();

            Random rnd = new Random();
            for (int i = 0; i < 31; i++)
            {
                Calendar.GetCalendar().AddDate((rnd.Next(0, 7) > 3) ? (new WeekendDayFactory().GetDate()) : (new WorkingDayFactory().GetDate()));
            }
        }

        private void buttonCalendar_Click(object sender, EventArgs e)
        {
            Command HideAll = new HideAllComponentsCommand();
            Command ShowComponent = new ShowComponentsCommand("Calendar");

            HideAll.Execute();
            ShowComponent.Execute();
        }
        private void buttonInventory_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            int counter = 0;

            for (int i = 0; counter < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                dataGridView1.Rows.Add();
                for (int j = 0; j < dataGridView1.ColumnCount && counter < Student.GetCurrentStudent().GetInventory().Labs.Count; j++)
                {
                    Image image = Image.FromFile(Student.GetCurrentStudent().GetInventory().Labs[counter++].Subject + ".jpg");
                    dataGridView1.Rows[i].Cells[j].Value = image;
                }
            }

            Command HideAll = new HideAllComponentsCommand();
            Command ShowComponent = new ShowComponentsCommand("Inventory");

            HideAll.Execute();
            ShowComponent.Execute();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex * dataGridView1.ColumnCount + dataGridView1.CurrentCell.ColumnIndex;
            if (index < Student.GetCurrentStudent().GetInventory().Labs.Count)
            {
                string message = "";
                message += "Topic: " + Student.GetCurrentStudent().GetInventory().Labs[index].Topic + "\n";
                message += "Subject: " + Student.GetCurrentStudent().GetInventory().Labs[index].Subject + "\n";
                message += "State: " + Student.GetCurrentStudent().GetInventory().Labs[index].CurrentLabState.State;
                MessageBox.Show(message);
            }

        }

        private void buttonDoLab_Click(object sender, EventArgs e)
        {
            DoLabContextMenu.Show(Cursor.Position);
        }

        /* ������ ���� */
        private void mAPZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Student.GetCurrentStudent().GetInventory().Labs.Count - 1; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i >= 0; i--)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "MAPZ" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Ready"
                    && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Passed")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.ImproveState(Student.GetCurrentStudent().GetInventory().Labs[i]);
                    return;
                }
            }

            Builder builder = new MAPZLabBuilder();
            builder.CreateNewLab();
            builder.SetAuthor();
            builder.SetNumber();
            builder.SetSubject();
            builder.SetTopic();
            Student.GetCurrentStudent().GetInventory().Labs.Add(builder.GetLab());

            buttonInventory_Click(sender, e);
        }

        private void oKMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Student.GetCurrentStudent().GetInventory().Labs.Count - 1; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i >= 0; i--)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "OKM" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Ready"
                    && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Passed")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.ImproveState(Student.GetCurrentStudent().GetInventory().Labs[i]);
                    return;
                }
            }

            Builder builder = new OKMLabBuilder();
            builder.CreateNewLab();
            builder.SetAuthor();
            builder.SetNumber();
            builder.SetSubject();
            builder.SetTopic();
            Student.GetCurrentStudent().GetInventory().Labs.Add(builder.GetLab());

            buttonInventory_Click(sender, e);
        }

        private void sMADToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Student.GetCurrentStudent().GetInventory().Labs.Count - 1; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i >= 0; i--)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "SMAD" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Ready"
                    && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State != "Passed")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.ImproveState(Student.GetCurrentStudent().GetInventory().Labs[i]);
                    return;
                }
            }

            Builder builder = new SMADLabBuilder();
            builder.CreateNewLab();
            builder.SetAuthor();
            builder.SetNumber();
            builder.SetSubject();
            builder.SetTopic();
            Student.GetCurrentStudent().GetInventory().Labs.Add(builder.GetLab());

            buttonInventory_Click(sender, e);
        }

        private void buttonShop_Click(object sender, EventArgs e)
        {
            ShopContextMenu.Show(Cursor.Position);
        }


        /* ������ ��� */
        private void mAPZToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade();
            ShopObserver shopObserver = new ShopObserver();
            facade.AttachObserver(shopObserver);
            facade.BuyOneLab("MAPZ");

            buttonInventory_Click(sender, e);

            Command update = new UpdateCommand();
            update.Execute();
        }

        private void oKMToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade();
            ShopObserver shopObserver = new ShopObserver();
            facade.AttachObserver(shopObserver);
            facade.BuyOneLab("OKM");

            buttonInventory_Click(sender, e);

            Command update = new UpdateCommand();
            update.Execute();
        }

        private void sMADToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade();
            ShopObserver shopObserver = new ShopObserver();
            facade.AttachObserver(shopObserver);
            facade.BuyOneLab("SMAD");

            buttonInventory_Click(sender, e);

            Command update = new UpdateCommand();
            update.Execute();
        }

        private void buySeveralToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command HideAll = new HideAllComponentsCommand();
            Command ShowComponent = new ShowComponentsCommand("Buy");

            HideAll.Execute();
            ShowComponent.Execute();
        }

        private void buttonBuy_Click(object sender, EventArgs e)
        {
            List<string> subjects = new List<string>();
            for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
            {
                subjects.Add(checkedListBox1.CheckedItems[i].ToString());
            }

            if (subjects.Count > 0)
            {
                Facade facade = new Facade();
                ShopObserver shopObserver = new ShopObserver();
                facade.AttachObserver(shopObserver);
                facade.BuySeveralLabs(subjects.ToArray());

                buttonInventory_Click(sender, e);

                Command update = new UpdateCommand();
                update.Execute();
            }

        }

        /* ������ ��� */
        private void sMADToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "MAPZ" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Facade facade = new Facade();
                    ShopObserver shopObserver = new ShopObserver();
                    facade.AttachObserver(shopObserver);
                    facade.SellOneLab(i);

                    buttonInventory_Click(sender, e);

                    Command update = new UpdateCommand();
                    update.Execute();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void oKMToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "OKM" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Facade facade = new Facade();
                    ShopObserver shopObserver = new ShopObserver();
                    facade.AttachObserver(shopObserver);
                    facade.SellOneLab(i);

                    buttonInventory_Click(sender, e);

                    Command update = new UpdateCommand();
                    update.Execute();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void sMADToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "SMAD" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Facade facade = new Facade();
                    ShopObserver shopObserver = new ShopObserver();
                    facade.AttachObserver(shopObserver);
                    facade.SellOneLab(i);

                    buttonInventory_Click(sender, e);

                    Command update = new UpdateCommand();
                    update.Execute();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void sellSeveralToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBoxSell.Items.Clear();

            int counter = 0;
            for (int i = 0; i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    comboBoxSell.Items.Add(++counter);
                }
            }

            Command HideAll = new HideAllComponentsCommand();
            Command ShowComponent = new ShowComponentsCommand("Sell");

            HideAll.Execute();
            ShowComponent.Execute();
        }

        private void buttonSell_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(comboBoxSell.SelectedItem.ToString());

            if(count > 0)
            {
                List<int> indexes = new List<int>();

                for (int i = 0; i < Student.GetCurrentStudent().GetInventory().Labs.Count && count > 0; i++)
                {
                    if(Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                    {
                        indexes.Add(i);
                        --count;
                    }
                }

                Facade facade = new Facade();
                ShopObserver shopObserver = new ShopObserver();
                facade.AttachObserver(shopObserver);
                facade.SellSeveralLabs(indexes.ToArray());

                buttonInventory_Click(sender, e);

                Command update = new UpdateCommand();
                update.Execute();
            }
        }

        private void buttonHandOut_Click(object sender, EventArgs e)
        {
            HandOutContextMenu.Show(Cursor.Position);
        }

        /* ����� ���� */

        private void mAPZToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "MAPZ" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState = new Passed();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void oKMToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "OKM" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState = new Passed();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void sMADToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            for (int i = 0; Student.GetCurrentStudent().GetInventory().Labs.Count != 0 && i < Student.GetCurrentStudent().GetInventory().Labs.Count; i++)
            {
                if (Student.GetCurrentStudent().GetInventory().Labs[i].Subject == "SMAD" && Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState.State == "Ready")
                {
                    Student.GetCurrentStudent().GetInventory().Labs[i].CurrentLabState = new Passed();
                    return;
                }
            }

            MessageBox.Show("You don't have right lab");
        }

        private void buttonLevelup_Click(object sender, EventArgs e)
        {
            StudentDecorator studentDecorator = new StudentDecorator(Student.GetCurrentStudent());
            studentDecorator.LevelUp();

            Command update = new UpdateCommand();
            update.Execute();
        }

        private void buttonStudy_Click(object sender, EventArgs e)
        {
            Student.GetCurrentStudent().GetInventory().Points += 50;

            Command update = new UpdateCommand();
            update.Execute();
        }

        /* ������������ */
        private void buttonSettings_Click(object sender, EventArgs e)
        {
            SettingsContextMenu.Show(Cursor.Position);
        }

        private void saveMementoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Memento memento = new Memento();
            memento.QuickSave();
            
        }

        private void loadMementoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command hideAll = new HideAllComponentsCommand();
            hideAll.Execute();

            Memento memento = new Memento();
            memento.QuickLoad();

            Command update = new UpdateCommand();
            update.Execute();
        }

        private void saveSerializationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Serialization serialization = new Serialization();
            serialization.Save();
        }

        private void loadSerializationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command hideAll = new HideAllComponentsCommand();
            hideAll.Execute();

            Serialization serialization = new Serialization();
            serialization.Load();

            Command update = new UpdateCommand();
            update.Execute();
        }
    }
}
