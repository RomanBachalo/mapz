﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{

    class Decorator : User
    {
        protected User CurrentUser;
        public Decorator(User user)
        {
            CurrentUser = user;
        }
    }
    
    class StudentDecorator : Decorator
    {
        public StudentDecorator(User user)
            :base(user)
        {
        }

        public void LevelUp()
        {
            if (((Student)CurrentUser).GetInventory().Points >= 100)
            {
                ((Student)CurrentUser).GetInventory().Points -= 100;
                ((Student)CurrentUser).Level++;


                ThreadSafeSingleton.GetInstance().Log( "Raising The Level");
                LazyLoadingSingleton.GetInstance().Log("Raising The Level");
            }
        }

        
    }

    
    /*-----------------------*/


    enum Difficulty
    {
        Low,
        Medium,
        High,
        Very_High
    }

    class Settings
    {
        private Difficulty CurrentDifficulty = Difficulty.Medium;

        public void SetDifficulty(Difficulty difficulty)
        {
            CurrentDifficulty = difficulty;
        }

        public Difficulty GetDifficulty()
        {
            return CurrentDifficulty;
        }

    }


    class AdvancedSettingsDecorator : Settings
    {
        void ChangeName(string name)
        {
            Student.GetCurrentStudent().Name = name;
        }

        void ChangeLevel(int level)
        {
            Student.GetCurrentStudent().Level = level;
        }
    }

}
