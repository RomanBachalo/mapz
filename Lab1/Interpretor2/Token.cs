﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretor
{
    public class Token
    {
        private string type;
        private string value;

        public Token(string t, string v)
        {
            type = t;
            value = v;
        }

        public string getType()
        {
            return type;
        }

        public string getValue()
        {
            return value;
        }
    }
}
