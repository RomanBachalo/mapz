﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Interpretor
{
    public partial class MainForm : Form
    {
        string inputHTMLCode = "";
        string commonTree = "";
        string optimizedTree = "";
        Dictionary<string, string> Values = new Dictionary<string, string>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "HTML code(*.html)|*.html";
            openFileDialog1.ShowDialog();
            string fileName = openFileDialog1.FileName;
            StreamReader file = new StreamReader(fileName);
            string lines;
            while((lines = file.ReadLine())!=null)
            {
                inputHTMLCode += lines;
                inputHTMLCode += "\n";
            }

            file.Close();
            richTextBoxHTML.Text = inputHTMLCode;
        }

        private void interpreteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (inputHTMLCode.Length == 0) 
            {
                MessageBox.Show("Open html first");
                return;
            }
            Values.Clear();
            richTextBoxOutput.Text = "";
            List<Token> toks = new List<Token>();
            toks = Lexer(richTextBoxPseudoCode.Text);
            List<Node> Tree = new List<Node>(Parser(toks, 0, toks.Count, 0));
            commonTree = "Program \n| \n| \n" + drawTree(Tree);
            optimizedTree = "Program \n| \n| \n" + drawTree(optimizeTree(Tree));

            executeCode(toks, 0, toks.Count);
            dataGridViewLexems.Rows.Clear();
            for (int i = 0; i < toks.Count; i++)
            {
                dataGridViewLexems.Rows.Add();
                dataGridViewLexems.Rows[i].Cells[0].Value = toks[i].getType();
                dataGridViewLexems.Rows[i].Cells[1].Value = toks[i].getValue();
            }
        }

        public List<Token> Lexer(string code)
        {
            List<Token> toks = new List<Token>();
            int i = 0;

            while(i < code.Length)
            {
                string word = "";

                while (i < code.Length && Convert.ToChar(code[i]) != ' ' && Convert.ToChar(code[i]) != '\n' &&
                    Convert.ToChar(code[i]) != '\t') 
                {
                    word += code[i++];
                }

                switch (word)
                {
                    case "loop":
                        toks.Add(new Token("Loop", word));
                        break;
                    case "if":
                        toks.Add(new Token("If", word));
                        break;
                    case "var":
                        toks.Add(new Token("Initialisation", word));
                        break;
                    case "{":
                        toks.Add(new Token("OpenBrace", word));
                        break;
                    case "}":
                        toks.Add(new Token("CloseBrace", word));
                        break;
                    default:
                        if(word.Contains('(') && word.Contains(')'))
                        {
                            toks.Add(new Token("Function", word));
                        }
                        else if(word=="<" || word == "<=" || word == ">" || word == ">=" 
                            || word == "==" || word == "!=")
                        {
                            toks.Add(new Token("Comparison", word));
                        }
                        else if((word.All(char.IsDigit)) || (word[0] == '-' && word.Substring(1).All(char.IsDigit)))
                        {
                            toks.Add(new Token("Value", word));
                        }
                        else
                        {
                            toks.Add(new Token("Name", word));
                        }
                        break;
                }
                word = "";
                i++;
            }

            for (int j = 0; j < toks.Count; j++)
            {
                if (toks[j].getValue() == "" || toks[j].getValue() == " " || toks[j].getValue() == "\t")
                {
                    toks.RemoveAt(j--);
                }
            }

            return toks;
        }

        public List<Node> Parser(List<Token> tok, int begin, int end, int baseLevel)
        {
            List<Node> TreeNodes = new List<Node>();
            int level = baseLevel;

            for (int i = begin; i < end; i++)
            {
                switch (tok[i].getType())
                {
                    case "Initialisation":
                        TreeNodes.Add(new Node("var", tok[i + 1].getValue(), tok[i + 2].getValue(), level));
                        i += 2;
                        break;
                    case "Function":
                        TreeNodes.Add(new Node("function", tok[i].getValue().Split('(')[0], tok[i].getValue().Split('(', ')')[1], level));
                        break;
                    case "If":
                        {
                            int beg = i + 5, en = beg, counter = 1;
                            while (counter != 0)
                            {
                                if (tok[en].getType() == "OpenBrace")
                                {
                                    ++counter;
                                }
                                else if (tok[en].getType() == "CloseBrace")
                                {
                                    --counter;
                                }
                                ++en;
                            }

                            TreeNodes.Add(new Node("if", tok[i + 1].getValue() + tok[i + 2].getValue() + tok[i + 3].getValue(),
                                Parser(tok, beg, en - 1, level + 1), level));
                            i = en - 1;
                            break;
                        }
                    case "Loop":
                        {
                            int beg = i + 3, en = beg, counter = 1;

                            while (counter != 0)
                            {
                                if (tok[en].getType() == "OpenBrace")
                                {
                                    ++counter;
                                }
                                else if (tok[en].getType() == "CloseBrace")
                                {
                                    --counter;
                                }
                                ++en;
                            }

                            TreeNodes.Add(new Node("loop", tok[i + 1].getValue(), Parser(tok, beg, en - 1, level + 1), level));
                            i = en - 1;
                            break;
                        }
                }
            }

            return TreeNodes;
        }

        public string drawTree(List<Node> TreeNodes)
        {
            string tree = "";

            foreach (var node in TreeNodes)
            {
                tree += "|";

                for (int i = 0; i < node.Level; i++)
                {
                    tree += "      |";
                }

                tree += "-" + node.Header + " _ " + node.Left + "\n|";

                if(node.Right != null)
                {
                    for (int i = 0; i < node.Level; i++)
                    {
                        tree += "      |";
                    }
                    for (int i = 0; i < node.Header.Length + 1; i++)
                    {
                        tree += " ";
                    }
                    tree += "\\_ " + node.Right + "\n|";
                    for (int i = 0; i < node.Level; i++)
                    {
                        tree += "      |";
                    }
                    tree += "\n";
                }
                else
                {
                    for (int i = 0; i < node.Level; i++)
                    {
                        tree += "      |";
                    }

                    for (int i = 0; i < node.Header.Length + 1; i++)
                    {
                        tree += " ";
                    }

                    tree += ((node.Header == "loop") ? "\\ \n|" : "\\__ \n|");

                    for (int i = 0; i < node.Level + 1; i++)
                    {
                        tree += "      |";
                    }

                    tree += "\n";
                    tree += drawTree(node.SubList);
                }
            }

            return tree;
        }

        public List<Node> optimizeTree(List<Node> tree)
        {
            List<Node> optTree = new List<Node>(tree);
            int varpos = -1, fpos = -1;

            for (int i = 0; i < optTree.Count; i++)
            {
                if (optTree[i].Header == "var")
                {
                    varpos = i;
                    break;
                }
            }
            
            if (varpos != -1)
            {
                for (int i = varpos + 1; i < optTree.Count; i++)
                {
                    if(optTree[i].Header == "var")
                    {
                        optTree[varpos] = optTree[varpos] + optTree[i];
                        optTree.RemoveAt(i--);
                    }
                }
            }

            for (int i = 0; i < optTree.Count; i++)
            {
                if (optTree[i].Header == "function")
                {
                    fpos = i;
                    break;
                }
            }

            if (fpos != -1)
            {
                for (int i = fpos + 1; i < optTree.Count; i++)
                {
                    if (optTree[i].Header == "function")
                    {
                        optTree[fpos] = optTree[fpos] + optTree[i];
                        optTree.RemoveAt(i--);
                    }
                }
            }

            for (int i = 0; i < optTree.Count; i++)
            {
                if (optTree[i].Right == null)
                {
                    optTree[i].SubList = optimizeTree(optTree[i].SubList);
                }
            }

            return optTree;
        }

        public void executeCode(List<Token> tokens, int begin, int end)
        {
            if(tokens.Count == 0)
            {
                MessageBox.Show("Enter code first");
                return;
            }

            while (begin < end)
            {
                switch (tokens[begin].getType())
                {
                    case "Loop":
                        int firstBrace, secondBrace, braceCounter = 1;
                        firstBrace = begin + 2;
                        secondBrace = begin + 3;
                        while (braceCounter != 0)
                        {
                            if (tokens[secondBrace].getType() == "OpenBrace")
                            {
                                ++braceCounter;
                            }
                            else if (tokens[secondBrace].getType() == "CloseBrace")
                            {
                                --braceCounter;
                            }

                            if (braceCounter == 0)
                            {
                                break;
                            }
                            else
                            {
                                ++secondBrace;
                            }
                        }

                        if (tokens[firstBrace - 1].getType() == "Name")
                        {
                            if (Values.ContainsKey(tokens[firstBrace - 1].getValue()))
                            {
                                string loopValue;
                                Values.TryGetValue(tokens[firstBrace - 1].getValue(), out loopValue);
                                for (int i = 0; i < Convert.ToInt32(loopValue); i++)
                                {
                                    executeCode(tokens, firstBrace + 1, secondBrace);
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < Convert.ToInt32(tokens[firstBrace - 1].getValue()); i++)
                            {
                                executeCode(tokens, firstBrace + 1, secondBrace);
                            }
                        }

                        begin = secondBrace + 1;

                        break;
                    case "If":
                        string left, right;
                        int l, r;

                        if(tokens[begin + 1].getType()=="Name")
                        {
                            Values.TryGetValue(tokens[begin + 1].getValue(), out left);
                        }
                        else
                        {
                            left = tokens[begin + 1].getValue();
                        }

                        if (tokens[begin + 3].getType() == "Name")
                        {
                            Values.TryGetValue(tokens[begin + 3].getValue(), out right);
                        }
                        else
                        {
                            right = tokens[begin + 3].getValue();
                        }

                        l = Convert.ToInt32(left);
                        r = Convert.ToInt32(right);
                        bool flag = false;

                        switch (tokens[begin + 2].getValue())
                        {
                            case "<":
                                if(l < r)
                                {
                                    flag = true;
                                }
                                break;
                            case "<=":
                                if (l <= r)
                                {
                                    flag = true;
                                }
                                break;
                            case ">":
                                if (l > r)
                                {
                                    flag = true;
                                }
                                break;
                            case ">=":
                                if (l >= r)
                                {
                                    flag = true;
                                }
                                break;
                            case "==":
                                if (l == r)
                                {
                                    flag = true;
                                }
                                break;
                            case "!=":
                                if (l != r)
                                {
                                    flag = true;
                                }
                                break;
                        }

                        int fstBrace, scndBrace, braceCount = 1;
                        fstBrace = begin + 4;
                        scndBrace = begin + 5;

                        while (braceCount != 0)
                        {
                            if (tokens[scndBrace].getType() == "OpenBrace")
                            {
                                ++braceCount;
                            }
                            else if (tokens[scndBrace].getType() == "CloseBrace")
                            {
                                --braceCount;
                            }

                            if (braceCount == 0)
                            {
                                break;
                            }
                            else
                            {
                                ++scndBrace;
                            }
                        }

                        if (flag)
                        {
                            executeCode(tokens, fstBrace + 1, scndBrace);
                        }

                        begin = scndBrace + 1;

                        break;
                    case "Initialisation":
                        if (!Values.ContainsKey(tokens[begin + 1].getValue()))
                        {
                            if (tokens[begin + 2].getType() == "Name")
                            {
                                if (Values.ContainsKey(tokens[begin + 2].getValue()))
                                {
                                    string val;
                                    Values.TryGetValue(tokens[begin + 2].getValue(), out val);
                                    Values.Add(tokens[begin + 1].getValue(), val);
                                }
                                else
                                {
                                    MessageBox.Show("Variable " + tokens[begin + 2].getValue() + " doesn`t exist");
                                    return;
                                }
                            }
                            else
                            {
                                Values.Add(tokens[begin + 1].getValue(), tokens[begin + 2].getValue());
                            }
                        }
                        else
                        {
                            MessageBox.Show("Variable " + tokens[begin + 1].getValue() + " already exists");
                        }

                        begin += 3;

                        break;
                    case "Function":
                        string[] nameAndArgs = tokens[begin].getValue().Split('(', ',', ')');
                        List<string> str = new List<string>(nameAndArgs);

                        for (int i = 0; i < str.Count; i++)
                        {
                            if (str[i] == "")
                            {
                                str.RemoveAt(i--);
                            }
                        }

                        nameAndArgs = str.ToArray();

                        switch (nameAndArgs[0])
                        {
                            case "findTag":
                                {
                                    List<int> open = new List<int>();       //індекси тегів для відкриття
                                    List<int> close = new List<int>();      //індекси тегів для закриття
                                    string tag = "<" + nameAndArgs[1], closingTag = "</" + nameAndArgs[1] + ">";

                                    for (int i = 0; i < inputHTMLCode.Length - tag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, tag.Length).Equals(tag))
                                        {
                                            open.Add(i);
                                        }
                                    }

                                    for (int i = 0; i < inputHTMLCode.Length - closingTag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, closingTag.Length).Equals(closingTag))
                                        {
                                            close.Add(i);
                                        }
                                    }

                                    string output = "";

                                    if (open.Count == 0)
                                    {
                                        MessageBox.Show("There are no " + tag + "> tag in text");
                                        break;
                                    }
                                    else if (open.Count > 1)
                                    {
                                        int index = -1;

                                        for (int i = 0; i < open.Count - 1; i++)
                                        {
                                            if (open[i] < close[i] && close[i] < open[i + 1])
                                            {
                                                ;
                                            }
                                            else
                                            {
                                                index = i;
                                            }

                                            if (index != -1)
                                            {
                                                open.RemoveAt(i + 1);
                                                close.RemoveAt(i);
                                                index = -1;
                                                --i;
                                            }
                                        }
                                    }

                                    for (int i = 0; i < open.Count; i++)
                                    {
                                        output += inputHTMLCode.Substring(open[i], close[i] - open[i] + closingTag.Length) + "\n";
                                    }

                                    List<string> outputStrList = new List<string>(output.Split('\t', '\n'));
                                    for (int i = 0; i < outputStrList.Count; i++)
                                    {
                                        if (outputStrList[i] == "")
                                        {
                                            outputStrList.RemoveAt(i--);
                                        }
                                    }

                                    foreach (var item in outputStrList)
                                    {
                                        richTextBoxOutput.Text += item;
                                        richTextBoxOutput.Text += "\n";
                                    }

                                    richTextBoxOutput.Text += "\n";

                                    break;
                                }
                            case "findFirstTag":
                                {
                                    List<int> open = new List<int>();       //індекси тегів для відкриття
                                    List<int> close = new List<int>();      //індекси тегів для закриття
                                    string tag = "<" + nameAndArgs[1], closingTag = "</" + nameAndArgs[1] + ">";

                                    for (int i = 0; i < inputHTMLCode.Length - tag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, tag.Length).Equals(tag))
                                        {
                                            open.Add(i);
                                        }
                                    }

                                    for (int i = 0; i < inputHTMLCode.Length - closingTag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, closingTag.Length).Equals(closingTag))
                                        {
                                            close.Add(i);
                                        }
                                    }

                                    string output = "";

                                    if (open.Count == 0)
                                    {
                                        MessageBox.Show("There are no " + tag + "> tag in text");
                                        break;
                                    }
                                    else if (open.Count > 1)
                                    {
                                        int index = -1;

                                        for (int i = 0; i < open.Count - 1; i++)
                                        {
                                            if (open[i] < close[i] && close[i] < open[i + 1])
                                            {
                                                ;
                                            }
                                            else
                                            {
                                                index = i;
                                            }

                                            if (index != -1)
                                            {
                                                open.RemoveAt(i + 1);
                                                close.RemoveAt(i);
                                                index = -1;
                                                --i;
                                            }

                                        }
                                    }

                                    output += inputHTMLCode.Substring(open[0], close[0] - open[0] + closingTag.Length) + "\n";
                                    List<string> outputStrList = new List<string>(output.Split('\t', '\n'));

                                    for (int i = 0; i < outputStrList.Count; i++)
                                    {
                                        if (outputStrList[i] == "")
                                        {
                                            outputStrList.RemoveAt(i--);
                                        }
                                    }

                                    foreach (var item in outputStrList)
                                    {
                                        richTextBoxOutput.Text += item;
                                        richTextBoxOutput.Text += "\n";
                                    }

                                    richTextBoxOutput.Text += "\n";

                                    break;
                                }
                            case "findLastTag":
                                {
                                    List<int> open = new List<int>();       //індекси тегів для відкриття
                                    List<int> close = new List<int>();      //індекси тегів для закриття
                                    string tag = "<" + nameAndArgs[1], closingTag = "</" + nameAndArgs[1] + ">";

                                    for (int i = 0; i < inputHTMLCode.Length - tag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, tag.Length).Equals(tag))
                                        {
                                            open.Add(i);
                                        }
                                    }

                                    for (int i = 0; i < inputHTMLCode.Length - closingTag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, closingTag.Length).Equals(closingTag))
                                        {
                                            close.Add(i);
                                        }
                                    }

                                    string output = "";

                                    if (open.Count == 0)
                                    {
                                        MessageBox.Show("There are no " + tag + "> tag in text");
                                        break;
                                    }
                                    else if (open.Count > 1)
                                    {
                                        int index = -1;

                                        for (int i = 0; i < open.Count - 1; i++)
                                        {
                                            if (open[i] < close[i] && close[i] < open[i + 1])
                                            {
                                                ;
                                            }
                                            else
                                            {
                                                index = i;
                                            }

                                            if (index != -1)
                                            {
                                                open.RemoveAt(i + 1);
                                                close.RemoveAt(i);
                                                index = -1;
                                                --i;
                                            }

                                        }
                                    }

                                    output += inputHTMLCode.Substring(open[open.Count - 1], close[close.Count - 1] -
                                        open[open.Count - 1] + closingTag.Length) + "\n";
                                    List<string> outputStrList = new List<string>(output.Split('\t', '\n'));

                                    for (int i = 0; i < outputStrList.Count; i++)
                                    {
                                        if (outputStrList[i] == "")
                                        {
                                            outputStrList.RemoveAt(i--);
                                        }
                                    }

                                    foreach (var item in outputStrList)
                                    {
                                        richTextBoxOutput.Text += item;
                                        richTextBoxOutput.Text += "\n";
                                    }

                                    richTextBoxOutput.Text += "\n";

                                    break;
                                }
                            case "findById":
                                {
                                    string id = nameAndArgs[1];
                                    int idPos = inputHTMLCode.IndexOf(id), tagStartPos = 0, ind = 0, closeIndex = 0;

                                    for (int i = idPos; i > 0; i--)
                                    {
                                        if(inputHTMLCode[i]=='<')
                                        {
                                            tagStartPos = i;
                                            break;
                                        }
                                    }

                                    ind = tagStartPos + 1;
                                    string tag = "<", closingTag;

                                    while (true)
                                    {
                                        if(inputHTMLCode[ind] == ' ' || inputHTMLCode[ind] == '>')
                                        {
                                            break;
                                        }
                                        tag += inputHTMLCode[ind++];
                                    }

                                    closingTag = tag + ">";
                                    closingTag = closingTag.Insert(1, "/");
                                    List<int> open = new List<int>();       //індекси тегів для відкриття
                                    List<int> close = new List<int>();      //індекси тегів для закриття

                                    for (int i = 0; i < inputHTMLCode.Length - tag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, tag.Length).Equals(tag))
                                        {
                                            open.Add(i);
                                        }
                                    }

                                    for (int i = 0; i < inputHTMLCode.Length - closingTag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, closingTag.Length).Equals(closingTag))
                                        {
                                            close.Add(i);
                                        }
                                    }

                                    string output = "";

                                    if (open.Count == 0)
                                    {
                                        MessageBox.Show("There are no " + tag + "> tag in text");
                                        break;
                                    }
                                    else if (open.Count > 1)
                                    {
                                        int ind1 = 0, ind2 = 0;

                                        for (int i = 0; i < open.Count; i++)
                                        {
                                            if (open[i] == tagStartPos)
                                            {
                                                ind1 = i;
                                                break;
                                            }
                                        }

                                        for (int i = 0; i < close.Count; i++)
                                        {
                                            if (close[i] > open[ind1])
                                            {
                                                ind2 = i;
                                                break;
                                            }
                                        }

                                        for (int i = 0; i < ind1; i++)
                                        {
                                            open.RemoveAt(0);
                                        }

                                        for (int i = 0; i < ind2; i++)
                                        {
                                            close.RemoveAt(0);
                                        }

                                        if (open.Count == 1 && close.Count == 1)
                                        {
                                            closeIndex = close[0];
                                        }
                                        else
                                        {
                                            int counter = 1, current = open[0];
                                            ind1 = 1;
                                            ind2 = 0;

                                            while (counter != 0)
                                            {
                                                if (open[ind1] < close[ind2])
                                                {
                                                    ++counter;
                                                    if (ind1 < open.Count - 1)
                                                    {
                                                        ++ind1;
                                                    }
                                                }
                                                else if (open[ind1] > close[ind2] || ind1 == open.Count - 1)
                                                {
                                                    --counter;
                                                    ++ind2;
                                                }

                                                if (counter == 0)
                                                {
                                                    closeIndex = close[ind2 - 1];
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    output += inputHTMLCode.Substring(tagStartPos, closeIndex - tagStartPos + closingTag.Length) + "\n";
                                    List<string> outputStrList = new List<string>(output.Split('\t', '\n'));

                                    for (int i = 0; i < outputStrList.Count; i++)
                                    {
                                        if (outputStrList[i] == "")
                                        {
                                            outputStrList.RemoveAt(i--);
                                        }
                                    }

                                    foreach (var item in outputStrList)
                                    {
                                        richTextBoxOutput.Text += item;
                                        richTextBoxOutput.Text += "\n";
                                    }

                                    richTextBoxOutput.Text += "\n";

                                    break;
                                }
                            case "insertAfter":
                                {
                                    string id = nameAndArgs[2], tagToInsert = nameAndArgs[1];
                                    int idPos = inputHTMLCode.IndexOf(id), tagStartPos = 0, ind = 0, closeIndex = 0;

                                    for (int i = idPos; i > 0; i--)
                                    {
                                        if (inputHTMLCode[i] == '<')
                                        {
                                            tagStartPos = i;
                                            break;
                                        }
                                    }

                                    ind = tagStartPos + 1;
                                    string tag = "<", closingTag;

                                    while (true)
                                    {
                                        if (inputHTMLCode[ind] == ' ' || inputHTMLCode[ind] == '>')
                                        {
                                            break;
                                        }
                                        tag += inputHTMLCode[ind++];
                                    }

                                    closingTag = tag + ">";
                                    closingTag = closingTag.Insert(1, "/");
                                    List<int> open = new List<int>();       //індекси тегів для відкриття
                                    List<int> close = new List<int>();      //індекси тегів для закриття

                                    for (int i = 0; i < inputHTMLCode.Length - tag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, tag.Length).Equals(tag))
                                        {
                                            open.Add(i);
                                        }
                                    }

                                    for (int i = 0; i < inputHTMLCode.Length - closingTag.Length + 1; i++)
                                    {
                                        if (inputHTMLCode.Substring(i, closingTag.Length).Equals(closingTag))
                                        {
                                            close.Add(i);
                                        }
                                    }

                                    if (open.Count == 0)
                                    {
                                        MessageBox.Show("There are no " + tag + "> tag in text");
                                        break;
                                    }
                                    else if (open.Count > 1)
                                    {
                                        int ind1 = 0, ind2 = 0;
                                        for (int i = 0; i < open.Count; i++)
                                        {
                                            if (open[i] == tagStartPos)
                                            {
                                                ind1 = i;
                                                break;
                                            }
                                        }

                                        for (int i = 0; i < close.Count; i++)
                                        {
                                            if (close[i] > open[ind1])
                                            {
                                                ind2 = i;
                                                break;
                                            }
                                        }

                                        for (int i = 0; i < ind1; i++)
                                        {
                                            open.RemoveAt(0);
                                        }
                                        for (int i = 0; i < ind2; i++)
                                        {
                                            close.RemoveAt(0);
                                        }

                                        int counter = 1, current = open[0];
                                        ind1 = 1;
                                        ind2 = 0;

                                        while (counter != 0)
                                        {
                                            if (open[ind1] < close[ind2])
                                            {
                                                ++counter;
                                                if (ind1 < open.Count - 1)
                                                {
                                                    ++ind1;
                                                }
                                            }
                                            else if (open[ind1] > close[ind2] || ind1 == open.Count - 1)
                                            {
                                                --counter;
                                                ++ind2;
                                            }

                                            if (counter == 0)
                                            {
                                                closeIndex = close[ind2 - 1];
                                                break;
                                            }
                                        }
                                    }

                                    inputHTMLCode = inputHTMLCode.Insert(closeIndex + closingTag.Length, "\n<" +
                                        tagToInsert + ">\n\n</" + tagToInsert + ">\n");
                                    richTextBoxHTML.Text = inputHTMLCode;

                                    break;
                                }
                            case "insertInto":
                                {
                                    string id = nameAndArgs[2], tagToInsert = nameAndArgs[1];
                                    int idPos = inputHTMLCode.IndexOf(id);
                                    int tagEndPos = (inputHTMLCode.Substring(idPos)).IndexOf(">") + idPos;
                                    inputHTMLCode = inputHTMLCode.Insert(tagEndPos + 1, "\n<" +
                                        tagToInsert + ">\n\n</" + tagToInsert + ">\n");
                                    richTextBoxHTML.Text = inputHTMLCode;

                                    break;
                                }
                        }
                        ++begin;
                        break;
                }
            }
        }

        private void showTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            writeTree(commonTree);
            System.Diagnostics.Process.Start("notepad.exe", "tree.txt");
        }

        private void showOptimizedTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            writeTree(optimizedTree);
            System.Diagnostics.Process.Start("notepad.exe", "tree.txt");
        }

        public void writeTree(string tree)
        {
            StreamWriter file = new StreamWriter("tree.txt");
            file.Write(tree);
            file.Close();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe", "Help.txt");
        }
    }
}