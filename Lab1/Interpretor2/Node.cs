﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpretor
{
    public class Node
    {
        private string header;
        private string left;
        private string right;
        private List<Node> subList;
        private int level;

        public string Header { get => header; set => header = value; }
        public string Left { get => left; set => left = value; }
        public string Right { get => right; set => right = value; }
        public List<Node> SubList { get => subList; set => subList = value; }
        public int Level { get => level; set => level = value; }

        public Node(Node n)
        {
            Header = n.Header;
            Left = n.Left;
            Right = n.Right;
            Level = n.Level;
        }

        public Node(string h, string l, string r, int lev)
        {
            Header = h;
            Left = l;
            Right = r;
            Level = lev;
        }

        public Node(string h, string l, List<Node> list, int lev)
        {
            Header = h;
            Left = l;
            SubList = new List<Node>(list);
            Level = lev;
        }

        public static Node operator+(Node l, Node r)
        {
            Node result = new Node(l);
            result.Left += " & " + r.Left;
            result.Right += " & " + r.Right;
            return result;
        }
    }
}
