﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Subject
    { }
    public class Order
    {
        public string Operation { get; set; }
        public int LabCount { get; set; }
    }

    class Facade : Subject
    {
        const int labPrice = 10;

        public Observer observer;
        public Order order = new Order();

        void Notify()
        {
            observer.Update(this);
        }

        public void AttachObserver(Observer _observer)
        {
            observer = _observer;
        }

        void DetachObserver(Observer _observer)
        {

        }

        public void BuyOneLab(string subject)
        {
            ThreadSafeSingleton.GetInstance().Log("Buying New Lab");
            LazyLoadingSingleton.GetInstance().Log("Buying New Lab");

            order.Operation = "Buy";
            order.LabCount = 1;
            Notify();

            var buyer = new Shop.BuyLabs();
            var shop = new Shop();
            switch (subject)
            {
                case "MAPZ":
                    if (shop.GetPoints() >= labPrice)
                    {
                        shop.AddLabToList(buyer.BuyMAPZLab());
                        shop.SubPoints(labPrice);
                    }
                    break;
                case "OKM":
                    if (shop.GetPoints() >= labPrice)
                    {
                        shop.AddLabToList(buyer.BuyOKMLab());
                        shop.SubPoints(labPrice);
                    }
                    break;
                case "SMAD":
                    if (shop.GetPoints() >= labPrice)
                    {
                        shop.AddLabToList(buyer.BuySMADLab());
                        shop.SubPoints(labPrice);
                    }
                    break;
                default:
                    break;
            }
        }

        public void BuySeveralLabs(string[] subjects)
        {
            ThreadSafeSingleton.GetInstance().Log("Buying" + Convert.ToString(subjects.Length) + " New Labs");
            LazyLoadingSingleton.GetInstance().Log("Buying" + Convert.ToString(subjects.Length) + " New Labs");

            order.Operation = "Buy";
            order.LabCount = subjects.Length;
            Notify();

            var buyer = new Shop.BuyLabs();
            var shop = new Shop();
            for (int i = 0; i < subjects.Length && shop.GetPoints() >= labPrice; i++)
            {
                switch (subjects[i])
                {
                    case "MAPZ":
                        shop.AddLabToList(buyer.BuyMAPZLab());
                        shop.SubPoints(labPrice);
                        break;

                    case "OKM":
                        shop.AddLabToList(buyer.BuyOKMLab());
                        shop.SubPoints(labPrice);
                        break;

                    case "SMAD":
                        shop.AddLabToList(buyer.BuySMADLab());
                        shop.SubPoints(labPrice);
                        break;

                    default:
                        break;
                }
            }
        }

        public void SellOneLab(int labIndex)
        {
            ThreadSafeSingleton.GetInstance().Log("Selling Lab");
            LazyLoadingSingleton.GetInstance().Log("Selling Lab");

            order.Operation = "Sell";
            order.LabCount = 1;
            Notify();

            var seller = new Shop.SellLabs();
            var shop = new Shop();
            if (shop.GetLabCount() > labIndex && labIndex >= 0)
            {
                seller.SellLab(labIndex);
                shop.AddPoints(labPrice);
            }
        }

        public void SellSeveralLabs(int[] labIndexes)
        {
            ThreadSafeSingleton.GetInstance().Log("Selling" + Convert.ToString(labIndexes.Length) + " Labs");
            LazyLoadingSingleton.GetInstance().Log("Selling" + Convert.ToString(labIndexes.Length) + " Labs");

            order.Operation = "Sell";
            order.LabCount = labIndexes.Length;
            Notify();

            var seller = new Shop.SellLabs();
            var shop = new Shop();
            int labCount = shop.GetLabCount();
            for (int i = 0; i < labIndexes.Length && labCount > labIndexes[i] && labIndexes[i] >= 0; i++)
            {
                seller.SellLab(labIndexes[i]);
                shop.AddPoints(labPrice);
                --labCount;
            }
        }
    }

    class Shop
    {
        public int GetLabCount()
        {
            return Student.GetCurrentStudent().GetInventory().Labs.Count;
        }

        public void AddLabToList(Lab newLab)
        {
            if (newLab != null)
            {
                Student.GetCurrentStudent().GetInventory().Labs.Add(newLab);
                ThreadSafeSingleton.GetInstance().Log("Adding Lab To List");
                LazyLoadingSingleton.GetInstance().Log("Adding Lab To List");

            }
        }

        public int GetPoints()
        {
            return Student.GetCurrentStudent().GetInventory().Points;
        }

        public void AddPoints(int points)
        {
            Student.GetCurrentStudent().GetInventory().Points += points;
        }
        public void SubPoints(int points)
        {
            if (GetPoints() >= points)
            {
                Student.GetCurrentStudent().GetInventory().Points -= points;
            }
        }

        public class BuyLabs
        {
            public Lab BuyMAPZLab()
            {
                var mapz = new MAPZLabBuilder();
                var buyLab = new LabDirector();
                buyLab.SetLabBuilder(mapz);
                buyLab.CreateLab();
                buyLab.GetLab().CurrentLabState = new Ready();
                return buyLab.GetLab();
            }
            public Lab BuyOKMLab()
            {
                var okm = new OKMLabBuilder();
                var buyLab = new LabDirector();
                buyLab.SetLabBuilder(okm);
                buyLab.CreateLab();
                buyLab.GetLab().CurrentLabState = new Ready();
                return buyLab.GetLab();
            }
            public Lab BuySMADLab()
            {
                var smad = new SMADLabBuilder();
                var buyLab = new LabDirector();
                buyLab.SetLabBuilder(smad);
                buyLab.CreateLab();
                buyLab.GetLab().CurrentLabState = new Ready();
                return buyLab.GetLab();
            }
        }

        public class SellLabs
        {
            public void SellLab(int labIndex)
            {
                Student.GetCurrentStudent().GetInventory().Labs.RemoveAt(labIndex);
                ThreadSafeSingleton.GetInstance().Log("Removing Lab From List");
                LazyLoadingSingleton.GetInstance().Log("Removing Lab From List");

            }
        }

    }

}
