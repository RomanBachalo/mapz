﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    public interface IAbstractFactory
    {
        Date GetDate();
    }

    class WorkingDayFactory : IAbstractFactory
    {
        public Date GetDate()
        {
            
            return new WorkingDay();
        }
    }

    class WeekendDayFactory : IAbstractFactory
    {
        public Date GetDate()
        {
            
            return new WeekendDay();
        }
    }

}
