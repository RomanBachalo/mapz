﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Patterns
{
    class Flyweight
    {
        public static Dictionary<string, Image> Images = new Dictionary<string, Image>();
        public static Image GetImage(string subject)
        {
            if (!Images.ContainsKey("MAPZ"))
            {
                Images.Add("MAPZ", Image.FromFile("MAPZ.jpg"));
            }
            if (!Images.ContainsKey("OKM"))
            {
                Images.Add("OKM", Image.FromFile("OKM.jpg"));
            }
            if (!Images.ContainsKey("SMAD"))
            {
                Images.Add("SMAD", Image.FromFile("SMAD.jpg"));
            }

            ThreadSafeSingleton.GetInstance().Log("Using Flyweight");
            LazyLoadingSingleton.GetInstance().Log("Using Flyweight");

            return Images[subject];
        }
    }
}
