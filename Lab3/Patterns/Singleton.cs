﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Patterns
{
    class ThreadSafeSingleton
    {
        private ThreadSafeSingleton() { }
        private static ThreadSafeSingleton SingletonInstance = null;
        public static readonly object locker = new object();
        public static ThreadSafeSingleton GetInstance()
        {
            lock (locker)
            {
                if (SingletonInstance == null)
                {
                    SingletonInstance = new ThreadSafeSingleton();
                }
            }
            return SingletonInstance;
        }
        public void Log(string message)
        {
            StreamWriter writer = File.AppendText("SafeLog.txt");
            writer.WriteLine(DateTime.Now.ToLongTimeString() + " | " + message);
            writer.Close();
        }
    }

    class LazyLoadingSingleton
    {
        private LazyLoadingSingleton() { }
        private static LazyLoadingSingleton SingletonInstance;
        public static LazyLoadingSingleton GetInstance()
        {
            if (SingletonInstance == null)
            {
                SingletonInstance = new LazyLoadingSingleton();
            }
            return SingletonInstance;
        }
        public void Log(string message)
        {
            StreamWriter writer = File.AppendText("LazyLog.txt");
            writer.WriteLine(DateTime.Now.ToLongTimeString() + " | " + message);
            writer.Close();
        }
    }
}
