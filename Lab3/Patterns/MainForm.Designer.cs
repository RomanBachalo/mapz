namespace Patterns
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonHandOut = new System.Windows.Forms.Button();
            this.buttonStudy = new System.Windows.Forms.Button();
            this.buttonDoLab = new System.Windows.Forms.Button();
            this.buttonShop = new System.Windows.Forms.Button();
            this.buttonInventory = new System.Windows.Forms.Button();
            this.buttonCalendar = new System.Windows.Forms.Button();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelPoints = new System.Windows.Forms.Label();
            this.CurrentMonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ColumnMAPZ = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColumnOKM = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColumnSMAD = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.DoLabContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mAPZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oKMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMADToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShopContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buyOneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mAPZToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oKMToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sMADToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.buySeveralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sellToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMADToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.oKMToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sMADToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.sellSeveralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.buttonBuy = new System.Windows.Forms.Button();
            this.HandOutContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mAPZToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.oKMToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.sMADToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonLevelup = new System.Windows.Forms.Button();
            this.comboBoxSell = new System.Windows.Forms.ComboBox();
            this.buttonSell = new System.Windows.Forms.Button();
            this.SettingsContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveMementoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMementoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSerializationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSerializationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.DoLabContextMenu.SuspendLayout();
            this.ShopContextMenu.SuspendLayout();
            this.HandOutContextMenu.SuspendLayout();
            this.SettingsContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonHandOut
            // 
            this.buttonHandOut.Location = new System.Drawing.Point(12, 12);
            this.buttonHandOut.Name = "buttonHandOut";
            this.buttonHandOut.Size = new System.Drawing.Size(120, 40);
            this.buttonHandOut.TabIndex = 0;
            this.buttonHandOut.Tag = "DoNotHide";
            this.buttonHandOut.Text = "Hand Out";
            this.buttonHandOut.UseVisualStyleBackColor = true;
            this.buttonHandOut.Click += new System.EventHandler(this.buttonHandOut_Click);
            // 
            // buttonStudy
            // 
            this.buttonStudy.Location = new System.Drawing.Point(141, 12);
            this.buttonStudy.Name = "buttonStudy";
            this.buttonStudy.Size = new System.Drawing.Size(120, 40);
            this.buttonStudy.TabIndex = 1;
            this.buttonStudy.Tag = "DoNotHide";
            this.buttonStudy.Text = "Study";
            this.buttonStudy.UseVisualStyleBackColor = true;
            this.buttonStudy.Click += new System.EventHandler(this.buttonStudy_Click);
            // 
            // buttonDoLab
            // 
            this.buttonDoLab.Location = new System.Drawing.Point(271, 12);
            this.buttonDoLab.Name = "buttonDoLab";
            this.buttonDoLab.Size = new System.Drawing.Size(120, 40);
            this.buttonDoLab.TabIndex = 2;
            this.buttonDoLab.Tag = "DoNotHide";
            this.buttonDoLab.Text = "Do Lab";
            this.buttonDoLab.UseVisualStyleBackColor = true;
            this.buttonDoLab.Click += new System.EventHandler(this.buttonDoLab_Click);
            // 
            // buttonShop
            // 
            this.buttonShop.Location = new System.Drawing.Point(661, 12);
            this.buttonShop.Name = "buttonShop";
            this.buttonShop.Size = new System.Drawing.Size(120, 40);
            this.buttonShop.TabIndex = 5;
            this.buttonShop.Tag = "DoNotHide";
            this.buttonShop.Text = "Shop";
            this.buttonShop.UseVisualStyleBackColor = true;
            this.buttonShop.Click += new System.EventHandler(this.buttonShop_Click);
            // 
            // buttonInventory
            // 
            this.buttonInventory.Location = new System.Drawing.Point(531, 12);
            this.buttonInventory.Name = "buttonInventory";
            this.buttonInventory.Size = new System.Drawing.Size(120, 40);
            this.buttonInventory.TabIndex = 4;
            this.buttonInventory.Tag = "DoNotHide";
            this.buttonInventory.Text = "Inventory";
            this.buttonInventory.UseVisualStyleBackColor = true;
            this.buttonInventory.Click += new System.EventHandler(this.buttonInventory_Click);
            // 
            // buttonCalendar
            // 
            this.buttonCalendar.Location = new System.Drawing.Point(400, 12);
            this.buttonCalendar.Name = "buttonCalendar";
            this.buttonCalendar.Size = new System.Drawing.Size(120, 40);
            this.buttonCalendar.TabIndex = 3;
            this.buttonCalendar.Tag = "DoNotHide";
            this.buttonCalendar.Text = "Calendar";
            this.buttonCalendar.UseVisualStyleBackColor = true;
            this.buttonCalendar.Click += new System.EventHandler(this.buttonCalendar_Click);
            // 
            // labelLevel
            // 
            this.labelLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLevel.AutoSize = true;
            this.labelLevel.Location = new System.Drawing.Point(1120, 9);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(42, 17);
            this.labelLevel.TabIndex = 7;
            this.labelLevel.Tag = "DoNotHide";
            this.labelLevel.Text = "Level";
            // 
            // labelPoints
            // 
            this.labelPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPoints.AutoSize = true;
            this.labelPoints.Location = new System.Drawing.Point(1120, 39);
            this.labelPoints.Name = "labelPoints";
            this.labelPoints.Size = new System.Drawing.Size(47, 17);
            this.labelPoints.TabIndex = 8;
            this.labelPoints.Tag = "DoNotHide";
            this.labelPoints.Text = "Points";
            // 
            // CurrentMonthCalendar
            // 
            this.CurrentMonthCalendar.Location = new System.Drawing.Point(12, 64);
            this.CurrentMonthCalendar.MaxSelectionCount = 1;
            this.CurrentMonthCalendar.Name = "CurrentMonthCalendar";
            this.CurrentMonthCalendar.ShowToday = false;
            this.CurrentMonthCalendar.ShowTodayCircle = false;
            this.CurrentMonthCalendar.TabIndex = 10;
            this.CurrentMonthCalendar.Tag = "Calendar";
            this.CurrentMonthCalendar.Visible = false;
            this.CurrentMonthCalendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.CurrentMonthCalendar_DateSelected);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnMAPZ,
            this.ColumnOKM,
            this.ColumnSMAD});
            this.dataGridView1.Location = new System.Drawing.Point(12, 64);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 40;
            this.dataGridView1.Size = new System.Drawing.Size(459, 481);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.Tag = "Inventory";
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ColumnMAPZ
            // 
            this.ColumnMAPZ.HeaderText = "MAPZ";
            this.ColumnMAPZ.Name = "ColumnMAPZ";
            this.ColumnMAPZ.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnMAPZ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnOKM
            // 
            this.ColumnOKM.HeaderText = "OKM";
            this.ColumnOKM.Name = "ColumnOKM";
            this.ColumnOKM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnOKM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnSMAD
            // 
            this.ColumnSMAD.HeaderText = "SMAD";
            this.ColumnSMAD.Name = "ColumnSMAD";
            this.ColumnSMAD.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnSMAD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "MAPZ";
            this.dataGridViewImageColumn1.Image = global::Patterns.Properties.Resources.MAPZ;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DoLabContextMenu
            // 
            this.DoLabContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.DoLabContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mAPZToolStripMenuItem,
            this.oKMToolStripMenuItem,
            this.sMADToolStripMenuItem});
            this.DoLabContextMenu.Name = "DoLabContextMenu";
            this.DoLabContextMenu.Size = new System.Drawing.Size(121, 76);
            // 
            // mAPZToolStripMenuItem
            // 
            this.mAPZToolStripMenuItem.Name = "mAPZToolStripMenuItem";
            this.mAPZToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.mAPZToolStripMenuItem.Text = "MAPZ";
            this.mAPZToolStripMenuItem.Click += new System.EventHandler(this.mAPZToolStripMenuItem_Click);
            // 
            // oKMToolStripMenuItem
            // 
            this.oKMToolStripMenuItem.Name = "oKMToolStripMenuItem";
            this.oKMToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.oKMToolStripMenuItem.Text = "OKM";
            this.oKMToolStripMenuItem.Click += new System.EventHandler(this.oKMToolStripMenuItem_Click);
            // 
            // sMADToolStripMenuItem
            // 
            this.sMADToolStripMenuItem.Name = "sMADToolStripMenuItem";
            this.sMADToolStripMenuItem.Size = new System.Drawing.Size(120, 24);
            this.sMADToolStripMenuItem.Text = "SMAD";
            this.sMADToolStripMenuItem.Click += new System.EventHandler(this.sMADToolStripMenuItem_Click);
            // 
            // ShopContextMenu
            // 
            this.ShopContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ShopContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buyOneToolStripMenuItem,
            this.buySeveralToolStripMenuItem,
            this.sellToolStripMenuItem,
            this.sellSeveralToolStripMenuItem});
            this.ShopContextMenu.Name = "ShopContextMenu";
            this.ShopContextMenu.Size = new System.Drawing.Size(155, 100);
            // 
            // buyOneToolStripMenuItem
            // 
            this.buyOneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mAPZToolStripMenuItem1,
            this.oKMToolStripMenuItem1,
            this.sMADToolStripMenuItem1});
            this.buyOneToolStripMenuItem.Name = "buyOneToolStripMenuItem";
            this.buyOneToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.buyOneToolStripMenuItem.Text = "Buy One";
            // 
            // mAPZToolStripMenuItem1
            // 
            this.mAPZToolStripMenuItem1.Name = "mAPZToolStripMenuItem1";
            this.mAPZToolStripMenuItem1.Size = new System.Drawing.Size(126, 26);
            this.mAPZToolStripMenuItem1.Text = "MAPZ";
            this.mAPZToolStripMenuItem1.Click += new System.EventHandler(this.mAPZToolStripMenuItem1_Click);
            // 
            // oKMToolStripMenuItem1
            // 
            this.oKMToolStripMenuItem1.Name = "oKMToolStripMenuItem1";
            this.oKMToolStripMenuItem1.Size = new System.Drawing.Size(126, 26);
            this.oKMToolStripMenuItem1.Text = "OKM";
            this.oKMToolStripMenuItem1.Click += new System.EventHandler(this.oKMToolStripMenuItem1_Click);
            // 
            // sMADToolStripMenuItem1
            // 
            this.sMADToolStripMenuItem1.Name = "sMADToolStripMenuItem1";
            this.sMADToolStripMenuItem1.Size = new System.Drawing.Size(126, 26);
            this.sMADToolStripMenuItem1.Text = "SMAD";
            this.sMADToolStripMenuItem1.Click += new System.EventHandler(this.sMADToolStripMenuItem1_Click);
            // 
            // buySeveralToolStripMenuItem
            // 
            this.buySeveralToolStripMenuItem.Name = "buySeveralToolStripMenuItem";
            this.buySeveralToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.buySeveralToolStripMenuItem.Text = "Buy Several";
            this.buySeveralToolStripMenuItem.Click += new System.EventHandler(this.buySeveralToolStripMenuItem_Click);
            // 
            // sellToolStripMenuItem
            // 
            this.sellToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sMADToolStripMenuItem2,
            this.oKMToolStripMenuItem2,
            this.sMADToolStripMenuItem3});
            this.sellToolStripMenuItem.Name = "sellToolStripMenuItem";
            this.sellToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.sellToolStripMenuItem.Text = "Sell";
            // 
            // sMADToolStripMenuItem2
            // 
            this.sMADToolStripMenuItem2.Name = "sMADToolStripMenuItem2";
            this.sMADToolStripMenuItem2.Size = new System.Drawing.Size(126, 26);
            this.sMADToolStripMenuItem2.Text = "MAPZ";
            this.sMADToolStripMenuItem2.Click += new System.EventHandler(this.sMADToolStripMenuItem2_Click);
            // 
            // oKMToolStripMenuItem2
            // 
            this.oKMToolStripMenuItem2.Name = "oKMToolStripMenuItem2";
            this.oKMToolStripMenuItem2.Size = new System.Drawing.Size(126, 26);
            this.oKMToolStripMenuItem2.Text = "OKM";
            this.oKMToolStripMenuItem2.Click += new System.EventHandler(this.oKMToolStripMenuItem2_Click);
            // 
            // sMADToolStripMenuItem3
            // 
            this.sMADToolStripMenuItem3.Name = "sMADToolStripMenuItem3";
            this.sMADToolStripMenuItem3.Size = new System.Drawing.Size(126, 26);
            this.sMADToolStripMenuItem3.Text = "SMAD";
            this.sMADToolStripMenuItem3.Click += new System.EventHandler(this.sMADToolStripMenuItem3_Click);
            // 
            // sellSeveralToolStripMenuItem
            // 
            this.sellSeveralToolStripMenuItem.Name = "sellSeveralToolStripMenuItem";
            this.sellSeveralToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.sellSeveralToolStripMenuItem.Text = "Sell Several";
            this.sellSeveralToolStripMenuItem.Click += new System.EventHandler(this.sellSeveralToolStripMenuItem_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "MAPZ",
            "OKM",
            "SMAD"});
            this.checkedListBox1.Location = new System.Drawing.Point(12, 93);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(110, 72);
            this.checkedListBox1.TabIndex = 14;
            this.checkedListBox1.Tag = "Buy";
            this.checkedListBox1.Visible = false;
            // 
            // buttonBuy
            // 
            this.buttonBuy.Location = new System.Drawing.Point(153, 102);
            this.buttonBuy.Name = "buttonBuy";
            this.buttonBuy.Size = new System.Drawing.Size(120, 40);
            this.buttonBuy.TabIndex = 15;
            this.buttonBuy.Tag = "Buy";
            this.buttonBuy.Text = "Buy";
            this.buttonBuy.UseVisualStyleBackColor = true;
            this.buttonBuy.Visible = false;
            this.buttonBuy.Click += new System.EventHandler(this.buttonBuy_Click);
            // 
            // HandOutContextMenu
            // 
            this.HandOutContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.HandOutContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mAPZToolStripMenuItem2,
            this.oKMToolStripMenuItem3,
            this.sMADToolStripMenuItem4});
            this.HandOutContextMenu.Name = "HandOutContextMenu";
            this.HandOutContextMenu.Size = new System.Drawing.Size(121, 76);
            // 
            // mAPZToolStripMenuItem2
            // 
            this.mAPZToolStripMenuItem2.Name = "mAPZToolStripMenuItem2";
            this.mAPZToolStripMenuItem2.Size = new System.Drawing.Size(120, 24);
            this.mAPZToolStripMenuItem2.Text = "MAPZ";
            this.mAPZToolStripMenuItem2.Click += new System.EventHandler(this.mAPZToolStripMenuItem2_Click);
            // 
            // oKMToolStripMenuItem3
            // 
            this.oKMToolStripMenuItem3.Name = "oKMToolStripMenuItem3";
            this.oKMToolStripMenuItem3.Size = new System.Drawing.Size(120, 24);
            this.oKMToolStripMenuItem3.Text = "OKM";
            this.oKMToolStripMenuItem3.Click += new System.EventHandler(this.oKMToolStripMenuItem3_Click);
            // 
            // sMADToolStripMenuItem4
            // 
            this.sMADToolStripMenuItem4.Name = "sMADToolStripMenuItem4";
            this.sMADToolStripMenuItem4.Size = new System.Drawing.Size(120, 24);
            this.sMADToolStripMenuItem4.Text = "SMAD";
            this.sMADToolStripMenuItem4.Click += new System.EventHandler(this.sMADToolStripMenuItem4_Click);
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(790, 12);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(120, 40);
            this.buttonSettings.TabIndex = 6;
            this.buttonSettings.Tag = "DoNotHide";
            this.buttonSettings.Text = "Settings";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // buttonLevelup
            // 
            this.buttonLevelup.Location = new System.Drawing.Point(1008, 12);
            this.buttonLevelup.Name = "buttonLevelup";
            this.buttonLevelup.Size = new System.Drawing.Size(106, 40);
            this.buttonLevelup.TabIndex = 16;
            this.buttonLevelup.Tag = "DoNotHide";
            this.buttonLevelup.Text = "LevelUP";
            this.buttonLevelup.UseVisualStyleBackColor = true;
            this.buttonLevelup.Click += new System.EventHandler(this.buttonLevelup_Click);
            // 
            // comboBoxSell
            // 
            this.comboBoxSell.FormattingEnabled = true;
            this.comboBoxSell.Location = new System.Drawing.Point(12, 111);
            this.comboBoxSell.Name = "comboBoxSell";
            this.comboBoxSell.Size = new System.Drawing.Size(121, 24);
            this.comboBoxSell.TabIndex = 17;
            this.comboBoxSell.Tag = "Sell";
            // 
            // buttonSell
            // 
            this.buttonSell.Location = new System.Drawing.Point(170, 102);
            this.buttonSell.Name = "buttonSell";
            this.buttonSell.Size = new System.Drawing.Size(120, 40);
            this.buttonSell.TabIndex = 18;
            this.buttonSell.Tag = "Sell";
            this.buttonSell.Text = "Sell";
            this.buttonSell.UseVisualStyleBackColor = true;
            this.buttonSell.Click += new System.EventHandler(this.buttonSell_Click);
            // 
            // SettingsContextMenu
            // 
            this.SettingsContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.SettingsContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveMementoToolStripMenuItem,
            this.loadMementoToolStripMenuItem,
            this.saveSerializationToolStripMenuItem,
            this.loadSerializationToolStripMenuItem});
            this.SettingsContextMenu.Name = "SettingsContextMenu";
            this.SettingsContextMenu.Size = new System.Drawing.Size(208, 100);
            // 
            // saveMementoToolStripMenuItem
            // 
            this.saveMementoToolStripMenuItem.Name = "saveMementoToolStripMenuItem";
            this.saveMementoToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.saveMementoToolStripMenuItem.Text = "Save (Memento)";
            this.saveMementoToolStripMenuItem.Click += new System.EventHandler(this.saveMementoToolStripMenuItem_Click);
            // 
            // loadMementoToolStripMenuItem
            // 
            this.loadMementoToolStripMenuItem.Name = "loadMementoToolStripMenuItem";
            this.loadMementoToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.loadMementoToolStripMenuItem.Text = "Load (Memento)";
            this.loadMementoToolStripMenuItem.Click += new System.EventHandler(this.loadMementoToolStripMenuItem_Click);
            // 
            // saveSerializationToolStripMenuItem
            // 
            this.saveSerializationToolStripMenuItem.Name = "saveSerializationToolStripMenuItem";
            this.saveSerializationToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.saveSerializationToolStripMenuItem.Text = "Save (Serialization)";
            this.saveSerializationToolStripMenuItem.Click += new System.EventHandler(this.saveSerializationToolStripMenuItem_Click);
            // 
            // loadSerializationToolStripMenuItem
            // 
            this.loadSerializationToolStripMenuItem.Name = "loadSerializationToolStripMenuItem";
            this.loadSerializationToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.loadSerializationToolStripMenuItem.Text = "Load (Serialization)";
            this.loadSerializationToolStripMenuItem.Click += new System.EventHandler(this.loadSerializationToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 557);
            this.Controls.Add(this.buttonSell);
            this.Controls.Add(this.comboBoxSell);
            this.Controls.Add(this.buttonLevelup);
            this.Controls.Add(this.buttonBuy);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CurrentMonthCalendar);
            this.Controls.Add(this.labelPoints);
            this.Controls.Add(this.labelLevel);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.buttonShop);
            this.Controls.Add(this.buttonInventory);
            this.Controls.Add(this.buttonCalendar);
            this.Controls.Add(this.buttonDoLab);
            this.Controls.Add(this.buttonStudy);
            this.Controls.Add(this.buttonHandOut);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.Text = "Laborator";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.DoLabContextMenu.ResumeLayout(false);
            this.ShopContextMenu.ResumeLayout(false);
            this.HandOutContextMenu.ResumeLayout(false);
            this.SettingsContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonHandOut;
        private System.Windows.Forms.Button buttonStudy;
        private System.Windows.Forms.Button buttonDoLab;
        private System.Windows.Forms.Button buttonShop;
        private System.Windows.Forms.Button buttonInventory;
        private System.Windows.Forms.Button buttonCalendar;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelPoints;
        private System.Windows.Forms.MonthCalendar CurrentMonthCalendar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn ColumnMAPZ;
        private System.Windows.Forms.DataGridViewImageColumn ColumnOKM;
        private System.Windows.Forms.DataGridViewImageColumn ColumnSMAD;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ContextMenuStrip DoLabContextMenu;
        private System.Windows.Forms.ToolStripMenuItem mAPZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oKMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMADToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ShopContextMenu;
        private System.Windows.Forms.ToolStripMenuItem buyOneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mAPZToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oKMToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sMADToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buySeveralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sellToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMADToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem oKMToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sMADToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem sellSeveralToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button buttonBuy;
        private System.Windows.Forms.ContextMenuStrip HandOutContextMenu;
        private System.Windows.Forms.ToolStripMenuItem mAPZToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem oKMToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem sMADToolStripMenuItem4;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonLevelup;
        private System.Windows.Forms.ComboBox comboBoxSell;
        private System.Windows.Forms.Button buttonSell;
        private System.Windows.Forms.ContextMenuStrip SettingsContextMenu;
        private System.Windows.Forms.ToolStripMenuItem saveMementoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadMementoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSerializationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSerializationToolStripMenuItem;
    }
}

