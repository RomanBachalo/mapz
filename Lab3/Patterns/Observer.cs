﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patterns
{
    interface Observer
    {
        void Update(Subject subject);
    }

    class ShopObserver : Observer
    {
        public string message { get; set; } = String.Empty;

        public void Update(Subject subject)
        {
            var facade = (Facade)subject;
            message = facade.order.Operation + "ing ";
            message += facade.order.LabCount + " lab(s)";

            MessageBox.Show(message);

            ThreadSafeSingleton.GetInstance().Log("Using Observer");
            LazyLoadingSingleton.GetInstance().Log("Using Observer");
        }
    }
}
