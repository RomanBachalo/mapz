﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Patterns
{
    [Serializable]
    public class Save
    {
        public List<Lab> LabList { get; set; }
        public int Points { get; set; }
        public int Level { get; set; }

        public Save(List<Lab> labs, int points, int level)
        {
            LabList = new List<Lab>(labs);
            Points = points;
            Level = level;
        }
    }

    class Memento
    {
        public static List<Save> Saves = new List<Save>();

        public void QuickSave()
        {
            Saves.Add(new Save(Student.GetCurrentStudent().GetInventory().Labs, Student.GetCurrentStudent().GetInventory().Points, Student.GetCurrentStudent().Level));

            ThreadSafeSingleton.GetInstance().Log( "Saving Game With Memento");
            LazyLoadingSingleton.GetInstance().Log("Saving Game With Memento");
        }

        public void QuickLoad()
        {
            if (Saves.Count > 0)
            {
                Student.GetCurrentStudent().GetInventory().Labs = Saves[Saves.Count - 1].LabList;
                Student.GetCurrentStudent().GetInventory().Points = Saves[Saves.Count - 1].Points;
                Student.GetCurrentStudent().Level = Saves[Saves.Count - 1].Level;

                ThreadSafeSingleton.GetInstance().Log("Loading Game With Memento");
                LazyLoadingSingleton.GetInstance().Log("Loading Game With Memento");
            }

        }

        public void Load(int saveNumber)
        {
            if (saveNumber < Saves.Count)
            {
                Student.GetCurrentStudent().GetInventory().Labs = Saves[saveNumber].LabList;
                Student.GetCurrentStudent().GetInventory().Points = Saves[saveNumber].Points;
                Student.GetCurrentStudent().Level = Saves[saveNumber].Level;

                ThreadSafeSingleton.GetInstance().Log( "Loading Game With Memento");
                LazyLoadingSingleton.GetInstance().Log("Loading Game With Memento");
            }
        }

    }

    public class Serialization
    {
        public Save CurrentSave;

        public void Save()
        {
            CurrentSave = new Save(Student.GetCurrentStudent().GetInventory().Labs, Student.GetCurrentStudent().GetInventory().Points, Student.GetCurrentStudent().Level);

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("SerializationSave.txt", FileMode.Create, FileAccess.Write);

            formatter.Serialize(stream, CurrentSave);
            stream.Close();

            ThreadSafeSingleton.GetInstance().Log("Saving Game With Serialization");
            LazyLoadingSingleton.GetInstance().Log("Saving Game With Serialization");
        }

        public void Load()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("SerializationSave.txt", FileMode.Open, FileAccess.Read);

            CurrentSave = (Save)formatter.Deserialize(stream);

            Student.GetCurrentStudent().GetInventory().Labs = CurrentSave.LabList;
            Student.GetCurrentStudent().GetInventory().Points = CurrentSave.Points;
            Student.GetCurrentStudent().Level = CurrentSave.Level;

            ThreadSafeSingleton.GetInstance().Log("Loading Game With Serialization");
            LazyLoadingSingleton.GetInstance().Log("Loading Game With Serialization");
        }

    }

}
