﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    [Serializable]
    public class LabState
    {
        public string State { get; set; }
        public Lab CurrentLab;

        public LabState()
        {
            
        }

        public virtual void ImproveState(Lab lab)
        {
            
        }
    }

    [Serializable]
    public class NotReady : LabState
    {
        public NotReady()
            :base()
        {
            State = "Not Ready";
        }

        public override void ImproveState(Lab lab)
        {
            State = "Half Ready";
            lab.CurrentLabState = new HalfReady();
        }

        public void Overdue(Lab lab)
        {
            State = "Outstanding";
            lab.CurrentLabState = new Outstanding();
        }
    }

    [Serializable]
    public class HalfReady : LabState
    {
        public HalfReady()
            : base()
        {
            State = "Half Ready";
        }

        public override void ImproveState(Lab lab)
        {
            State = "Ready";
            lab.CurrentLabState = new Ready();
        }

        public void Overdue(Lab lab)
        {
            State = "Outstanding";
            lab.CurrentLabState = new Outstanding();
        }
    }

    [Serializable]
    public class Ready : LabState
    {
        public Ready()
            : base()
        {
            State = "Ready";
        }

        public void HandOver(Lab lab)
        {
            State = "Passed";
            lab.CurrentLabState = new Passed();
        }

        public void Overdue(Lab lab)
        {
            State = "Outstanding";
            lab.CurrentLabState = new Outstanding();
        }
    }

    [Serializable]
    public class Passed : LabState
    {
        public Passed()
            : base()
        {
            State = "Passed";
        }
    }

    //протермінована
    [Serializable]
    public class Outstanding : LabState
    {
        public Outstanding()
            : base()
        {
            State = "Outstanding";
        }

    }



}
