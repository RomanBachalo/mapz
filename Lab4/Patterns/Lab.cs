﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Patterns
{
    [Serializable]
    public class Lab
    {
        public int Number { get; set; }
        public string Topic { get; set; }
        public string Subject { get; set; } = "MAPZ";
        public Student Author { get; set; }
        public Image Picture;
        public LabState CurrentLabState { get; set; } = new NotReady();
        public Lab()
        {
            Picture = Flyweight.GetImage(Subject);
        }

        public void UpdatePicture()
        {
            Picture = Flyweight.GetImage(Subject);
        }
    }
}
