﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    public class Date
    {
        public DateTime Day { get; set; }
        public string Deadline { get; set; }
        public string reminder { get; set; }

        public virtual string GetStatus() { return "none"; }
    }

    class WorkingDay: Date
    {
        public override string GetStatus()
        {
            return "Working";
        }
    }

    class WeekendDay: Date
    {
        public override string GetStatus()
        {
            return "Relaxing";
        }
    }

    public class Calendar
    {
        private static Calendar CurrentCalendar = new Calendar();
        public List<Date> Dates;
        private Calendar()
        {
            Dates = new List<Date>();
        }
        public static Calendar GetCalendar()
        {
            return CurrentCalendar;
        }
        public void AddDate(Date date)
        {
            CurrentCalendar.Dates.Add(date);
        }
        public Date GetDate(int numberOfDate)
        {
            if (CurrentCalendar.Dates.Count > numberOfDate)
            {
                return CurrentCalendar.Dates[numberOfDate];
            }
            else
            {
                return null;
            }
        }
    }
}
