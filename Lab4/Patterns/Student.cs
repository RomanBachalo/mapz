﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    [Serializable]
    public class User
    {
    }

    [Serializable]
    public class Student : User
    {
        private static Student CurrentStudent = null;
        public string Name { get; set; }
        public int Level { get; set; } = 0;
        private Inventory StudentInventory { get; set; } = null;

        private Student() { }
        public static Student GetCurrentStudent()
        {
            if (CurrentStudent == null)
            {
                CurrentStudent = new Student();
                CurrentStudent.StudentInventory = new Inventory();
            }
            return CurrentStudent;
        }
        public Inventory GetInventory()
        {
            return StudentInventory;
        }
    }

    [Serializable]
    public class Inventory
    {
        public List<Lab> Labs { get; set; } = new List<Lab>();
        public int Points { get; set; } = 400;
    }
}
