﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    abstract class Builder
    {
        protected Lab CurrentLab;
        public Lab GetLab()
        {
            //CurrentLab.CurrentLabState = new Ready();
            CurrentLab.UpdatePicture();
            return CurrentLab;
        }

        public void CreateNewLab()
        {
            ThreadSafeSingleton.GetInstance().Log("Creating New Lab");
            LazyLoadingSingleton.GetInstance().Log("Creating New Lab");
            CurrentLab = new Lab();
            CurrentLab.CurrentLabState = new NotReady();
        }
        public abstract void SetNumber();
        public abstract void SetTopic();
        public abstract void SetSubject();
        public abstract void SetAuthor();
    }

    class MAPZLabBuilder : Builder
    {
        public override void SetNumber()
        {
            CurrentLab.Number = 10;
        }

        public override void SetTopic()
        {
            CurrentLab.Topic = "MAPZ topic";
        }

        public override void SetAuthor()
        {
            CurrentLab.Author = Student.GetCurrentStudent();
        }

        public override void SetSubject()
        {
            CurrentLab.Subject = "MAPZ";
        }
    }

    class OKMLabBuilder : Builder
    {
        public override void SetNumber()
        {
            CurrentLab.Number = 12;
        }

        public override void SetTopic()
        {
            CurrentLab.Topic = "OKM topic";
        }

        public override void SetAuthor()
        {
            CurrentLab.Author = Student.GetCurrentStudent();
        }

        public override void SetSubject()
        {
            CurrentLab.Subject = "OKM";
        }
    }

    class SMADLabBuilder : Builder
    {
        public override void SetNumber()
        {
            CurrentLab.Number = 3;
        }

        public override void SetTopic()
        {
            CurrentLab.Topic = "SMAD topic";
        }

        public override void SetAuthor()
        {
            CurrentLab.Author = Student.GetCurrentStudent();
        }

        public override void SetSubject()
        {
            CurrentLab.Subject = "SMAD";
        }

    }

    class LabDirector
    {
        private Builder labBuilder;
        public void SetLabBuilder(Builder lbuilder)
        {
            labBuilder = lbuilder;
        }

        public void CreateLab()
        {
            labBuilder.CreateNewLab();
            labBuilder.SetNumber();
            labBuilder.SetTopic();
            labBuilder.SetAuthor();
            labBuilder.SetSubject();

            ThreadSafeSingleton.GetInstance().Log("New Lab Created");
            LazyLoadingSingleton.GetInstance().Log("New Lab Created");
        }

        public Lab GetLab()
        {
            ThreadSafeSingleton.GetInstance().Log("Getting New Lab");
            LazyLoadingSingleton.GetInstance().Log("Getting New Lab");
            return labBuilder.GetLab();
        }
    }
}
